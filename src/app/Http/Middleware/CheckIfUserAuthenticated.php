<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfUserAuthenticated
{
    public function handle($request, Closure $next, $guard = null)
    {
        $auth = Auth::guard('web');
        if (!$auth->check()) {
            return redirect('/');
        }

        if ($auth->user()->deleted_at != null) {
            $auth->logout();

            return redirect()->back()
            ->with('error_code', 5)
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'info' => 'This Account has Banned',
            ]);
            
        }else if ($auth->user()->email_verified_at == null) {
            $auth->logout();
            
            return redirect()->back()
            ->with('error_code', 5)
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'info' => 'Please Confirm you account first',
            ]);
        }

        return $next($request);
    }
}
