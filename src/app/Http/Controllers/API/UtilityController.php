<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Subject;

class UtilityController extends Controller
{
    public function getCities($id)
    {
        $data = \Indonesia::findProvince($id, ['cities']);
        return response()->json($data);
    }

    public function getArticles()
    {
        // $data = Articles::
    }

    public function getSubjects($id)
    {
        $data = Subject::where('school_level_id',$id)->get();
        return response()->json($data);
    }
}
