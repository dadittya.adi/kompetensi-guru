<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Publication;

class PublicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }
    
    public function index(Request $request)
    {
        $message    = $request->session()->get('message');
        return view('backend.publication.index',compact('message'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data           = Publication::orderby('updated_at','desc');
            return datatables()->of($data)
            ->editColumn('is_active',function ($data)
            {
                if ($data->is_active) return '<span class="label label-flat label-rounded label-icon border-success text-success-600"><i class="icon-checkmark3"></i></span>';
            	else return '<span class="label label-flat label-rounded label-icon border-grey text-grey-600""><i class="icon-warning"></i></span>';
            })
            ->editColumn('published_date',function ($data)
            {
               
                if ($data->published_date)
                {
                    return $data->published_date->format('d/M/Y');
                } 
            	else return '';
            })
            ->addColumn('action', function($data) {
                return view('backend.publication._action', [
                    'model'     => $data,
                    'edit'      => route('backend.publication.edit',$data->id),
                    'delete'    => route('backend.publication.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('backend.publication.create');
    }

    public function store(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $published_date = Carbon::createFromFormat('d/m/Y', $request->published_date)->format('Y-m-d H:i:s');
        $title          = $request->title;
        $abstract       = $request->abstract;
        $is_active      = ($request->exists('is_active') ? true:false);
        $storage        = Config::get('storage.publication');
        $file           = $request->file('file');
        $_random        = null;

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($abstract, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $abstract = $dom->saveHTML();

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        if($file)
        {
            $random = Publication::generateFileName($file);
            $_random = $random[1];
        }

        $total_title = Publication::where('slug',str_slug($title))->count();


        $publication = Publication::create([
            'title'         => $title,
            'slug'          => str_slug($title.$total_title),
            'published_date'=> $published_date,
            'file'          => $_random,
            'abstract'      => $abstract,
            'is_active'     => $is_active,
        ]);

        if ($publication->save())
        {
            $file->move($storage, $random[1]);
        }

        $request->session()->flash('message', 'success');
        return response()->json('success',200);
    }

    public function edit(Request $request,$id)
    {
        $message    = $request->session()->get('message');
        $data       = Publication::find($id);
        $obj        = new StdClass();
       
        if($data->file)
        {
            $obj->caption       = route('backend.publication.showFile',$data->file);
            $obj->filename      = $data->file;
            $obj->downloadUrl   = route('backend.publication.showFile',$data->file);
            $obj->key           = 1;
            $array []           = $obj;
        }
        
        
        if($data) return view('backend.publication.edit',compact('data','array'));
        else return redirect()->action('Backend\PublicationController@index');
    }

    public function update(Request $request, $id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $published_date = Carbon::createFromFormat('d/m/Y', $request->published_date)->format('Y-m-d H:i:s');
        $title          = $request->title;
        $abstract       = $request->abstract;
        $is_modify      = $request->is_modify;
        $is_active      = ($request->exists('is_active') ? true:false);
        $storage        = Config::get('storage.publication');
        $file           = $request->file('file');
        $_random        = null;

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($abstract, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $abstract = $dom->saveHTML();

        $total_title            = Publication::where([
            ['id','!=',$id],
            ['slug',str_slug($title)]
        ])->count();

        $publication            = Publication::find($id);

        if($file && $is_modify == 1)
        {
            $random = Publication::generateFileName($file);
            $_random = $random[1];

            $old_file = $storage . '/' . $publication->file;
            if(File::exists($old_file)) File::delete($old_file);
        }else
        {
            $_random = $publication->file;
        }

        $publication->published_date    = $published_date;
        $publication->slug              = str_slug($title.$total_title);
        $publication->title             = $title;
        $publication->abstract          = $abstract;
        $publication->file              = $_random;
        $publication->is_active         = $is_active;

        if ($publication->save())
        {
            $request->session()->flash('message', 'success_2');
            if($file && $is_modify == 1) $file->move($storage, $_random);
        }
    }

    public function delete($id)
    {
        $publication    = Publication::find($id);
        $image          = $publication->getFileFullPath($publication->file);
        
        if(File::exists($image)) @unlink($image);

        $publication->delete();
    }

    public function showFile($filename)
    {
        $path = Config::get('storage.publication');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
