<?php namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Researchist;

class ResearcherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function index(Request $request)
    {
        $message    = $request->session()->get('message');
        return view('backend.researcher.index',compact('message'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $data           = Researchist::orderby('updated_at','desc');
            return datatables()->of($data)
            ->editColumn('is_active',function ($data)
            {
                if ($data->is_active) return '<span class="label label-flat label-rounded label-icon border-success text-success-600"><i class="icon-checkmark3"></i></span>';
            	else return '<span class="label label-flat label-rounded label-icon border-grey text-grey-600""><i class="icon-warning"></i></span>';
            })
            ->editColumn('image',function ($data)
            {
               
                if ($data->image)
                {
                    $url = route('backend.researcher.showImage',$data->image);
                    return '<img src="'.$url.'" class="img-circle img-md" alt="'.$data->name.'">';
                } 
            	else return '';
            })
            ->addColumn('action', function($data) {
                return view('backend.researcher._action', [
                    'model'     => $data,
                    'edit'      => route('backend.researcher.edit',$data->id),
                    'delete'    => route('backend.researcher.delete',$data->id),
                ]);
            })
            ->rawColumns(['action','is_active','image'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('backend.researcher.create');
    }

    public function store(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $name       = $request->name;
        $content    = $request->content;
        $is_active  = ($request->exists('is_active') ? true:false);
        $storage    = Config::get('storage.researcher');
        $image      = $request->file('image');
        $_random    = null;

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        if($image)
        {
            $random = Researchist::generateFileName($image);
            $_random = $random[1];
        }

        $researchist = Researchist::create([
            'name'          => $name,
            'image'         => $_random,
            'content'       => $content,
            'is_active'     => $is_active,
        ]);

        if ($researchist->save())
        {
            $image->move($storage, $random[1]);
        }

        $request->session()->flash('message', 'success');
        return response()->json('success',200);
    }

    public function edit(Request $request,$id)
    {
        $message    = $request->session()->get('message');
        $data       = Researchist::find($id);

        if($data) return view('backend.researcher.edit',compact('data'));
        else return redirect()->action('Backend\ResearcherController@index');
    }

    public function update(Request $request, $id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $name       = $request->name;
        $content    = $request->content;
        $is_modify  = $request->is_modify;
        $is_active  = ($request->exists('is_active') ? true:false);
        $storage    = Config::get('storage.researcher');
        $image      = $request->file('image');
        $_random    = null;

        if (!File::exists($storage)) File::makeDirectory($storage, 0777, true);

        $dom = new \DomDocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $content = $dom->saveHTML();

        $researchist            = Researchist::find($id);

        if($image && $is_modify == 1)
        {
            $random = Researchist::generateFileName($image);
            $_random = $random[1];

            $old_file = $storage . '/' . $researchist->image;
            if(File::exists($old_file)) File::delete($old_file);
        }else
        {
            $_random = $researchist->image;
        }

        $researchist->name          = $name;
        $researchist->content       = $content;
        $researchist->image         = $_random;
        $researchist->is_active     = $is_active;

        if ($researchist->save())
        {
            $request->session()->flash('message', 'success_2');
            if($image && $is_modify == 1) $image->move($storage, $_random);
        }
    }

    public function delete($id)
    {
        $researchist    = Researchist::find($id);
        $image          = $researchist->getImageFullPath($researchist->image);
        
        if(File::exists($image)) @unlink($image);

        $researchist->delete();
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.researcher');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
