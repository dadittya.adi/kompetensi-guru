<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Album;
use App\Models\Articles;
use App\Models\Publication;
use App\Models\Researchist;
use Config;

class LandingController extends Controller
{


    public function index()
    {
        $articles = Articles::where('is_active',true)
                            ->orderBy('id','desc')
                            ->paginate(3);

        return view('landing.home', compact('articles'));
    }

    public function articles()
    {
        $articles = Articles::where('is_active',true)
                            ->orderBy('id','desc')
                            ->paginate(6);

        return view('landing.article', compact('articles'));
    }

    public function showArticle($slug)
    {
        $article = Articles::where('slug', $slug)->first();

        return view('landing.single-article', compact('article'));
    }

    public function publications()
    {
        $publications = Publication::orderby('updated_at','desc')
                                    ->where('is_active',true)
                                    ->paginate(10);

        return view('landing.publikasi', compact('publications'));
    }

    public function showPublication($slug)
    {
        $publication = Publication::where('slug',$slug)->first();

        return view('landing.single-publikasi', compact('publication'));
    }

    public function hki()
    {
        return view('landing.hki');
    }

    public function researchist()
    {
        $researchists = Researchist::paginate(3);
        return view('landing.researchist', compact('researchists'));
    }

    public function faq()
    {
        return view('landing.faq');
    }

    public function gallery()
    {
        $galleries = Album::paginate(9);
        return view('landing.gallery', compact('galleries'));
    }

    public function about()
    {
        return view('landing.about');
    }

    public function showProfile()
    {
        return view('landing.about');
    }

    public function showImage($page, $filename)
    {
        /**
         * ========================================
         * Storage Name
         * ========================================
         * - gallery
         * - researcher
         * - article
         * - publication
         */

        $path = Config::get('storage.'.$page);
        $resp = response()->download($path . '/'. $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');

        return $resp;
    }
}
