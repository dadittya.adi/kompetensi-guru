<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Maps extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "path" => $this->path,
            "provinsi" => $this->provinsi,
            "guru" => $this->guru,
            "sekolah" => $this->sekolah,
            "yayasan" => $this->yayasan
        ];
    }
}
