<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Test extends Model
{
    protected $table        = 'tests';
    protected $fillable     = ['user_id'
        ,'test_number'
        ,'test_date'
        ,'type'
        ,'test_state'
        ,'status'
        ,'price'
        ,'paid_at'
    ];

    static function generateTestNumber()
    {
        $try        = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized test number");

            $test_number = strtoupper(static::cleanFilename(str_random(7)));

            $try -= 1;
        } while (static::isTestNumberExist($test_number));

        return $test_number;
    }

    static function isTestNumberExist($test_number)
    {
        return Test::where('test_number',$test_number)->exists();
    }

    static function cleanFilename($value)
    {
        $pattern        = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement    = '-';
        $value          = preg_replace($pattern, $replacement, $value);

        return $value;
    }

    static function getTotalDimension($value,$user_id,$test_id)
    {
        return DB::select(db::raw("
            SELECT tbl.dimension_id,
                tbl.video_url
            FROM
            (
                SELECT indicators.dimension_id,
                    dimensions.video_url,
                    COALESCE(sum(dtl.total),'0') as total
                FROM indicators
                JOIN dimensions on dimensions.id = indicators.dimension_id
                LEFT JOIN
                (
                    SELECT questionnaires.indicator_id,
                        sum(COALESCE(dtl.total,'0'))as total
                    FROM questionnaires
                    LEFT JOIN
                    (
                        SELECT questionnaire_id,
                            count(0) as total
                        FROM test_questionnaire
                        WHERE answer = 't'
                        AND EXISTS
                        (
                            SELECT 1
                            FROM tests
                            WHERE tests.id = test_questionnaire.test_id
                                AND user_id = $user_id
                                AND tests.id = '$test_id'
                        )
                        GROUP BY questionnaire_id
                    ) dtl on dtl.questionnaire_id = questionnaires.id
                    WHERE questionnaires.is_active = true
                    GROUP BY questionnaires.indicator_id
                ) dtl on dtl.indicator_id = indicators.id
                    AND indicators.is_active = true
                GROUP BY indicators.dimension_id
                ORDER BY dimension_id asc
            )tbl
            WHERE tbl.total = $value
        "));
    }

    static function getTotalIndicator($value,$user_id,$test_id)
    {
        return DB::select(db::raw("
            select tbl.*
            FROM
            (
                SELECT indicators.id as indicator_id,
                    dimensions.id as dimension_id,
                    indicators.video_url,
                    COALESCE(sum(dtl.total),'0') as total from indicators
                JOIN dimensions on dimensions.id = indicators.dimension_id
                LEFT JOIN
                (
                    SELECT questionnaires.indicator_id
                        ,sum(COALESCE(dtl.total,'0')) as total
                    FROM questionnaires
                    LEFT JOIN
                    (
                        SELECT questionnaire_id,
                            count(0) as total
                        FROM test_questionnaire
                        WHERE answer = 't'
                        AND EXISTS
                        (
                            SELECT 1
                            FROM tests
                            WHERE tests.id = test_questionnaire.test_id
                            AND user_id = $user_id
                            AND tests.id = '$test_id'
                        )
                        GROUP BY questionnaire_id
                    ) dtl on dtl.questionnaire_id = questionnaires.id
                    WHERE questionnaires.is_active = true
                    GROUP BY questionnaires.indicator_id
                ) dtl on dtl.indicator_id = indicators.id
                WHERE indicators.is_active = true
                GROUP BY indicators.id
                ORDER by dimension_id asc
            )tbl
            WHERE tbl.total = $value
        "));
    }
}
