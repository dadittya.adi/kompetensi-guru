<?php namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guarded      = ['admins'];
    protected $fillable     = ['name'
        ,'email'
        ,'email_verified_at'
        ,'password' 
    ];

    protected $hidden = [
        'password',
    ];

}
