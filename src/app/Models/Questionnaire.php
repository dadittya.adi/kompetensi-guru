<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $table        = 'questionnaires';
    protected $fillable     = ['indicator_id'
        ,'code'
        ,'name'
        ,'acronym'
        ,'order'
        ,'option'
        ,'pola_tc'
        ,'is_active'
    ];

    public function indicator()
    {
        return $this->belongsTo('App\Models\Indicator','indicator_id');
    }
}
