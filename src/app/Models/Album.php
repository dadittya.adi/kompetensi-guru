<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table        = 'albums';
    protected $fillable     = ['title'];

    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery');
    }
}
