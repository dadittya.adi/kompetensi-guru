<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherType extends Model
{
    protected $table        = 'teacher_types';
    protected $fillable     = ['name'
        ,'is_active'
    ];
}
