<?php namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $table        = 'articles';
    protected $fillable     = ['admin_id'
        ,'title'
        ,'slug'
        ,'image'
        ,'thumbnail'
        ,'content'
        ,'is_active'
    ];

    static function getImageFullPath($image)
    {
        return Config::get('storage.article') . '/' . e($image);
    }

    static function generateFileName($image)
    {
        $ret        = [];
        $path       = Config::get('storage.article');
        $extension  = $image->getClientOriginalExtension();
        $try        = 250;

        do {
            if ($try <= 0)
                throw Exception("Failed to produce randomized filename");

            $hash = static::cleanFilename(str_random(32));
            if ($extension)
                $hash = $hash . '.' . $extension;

            $file = $path . '/' . $hash;
            $try -= 1;
        } while (file_exists($file));

        $ret = [$path, $hash];
        return $ret;
    }

    static function cleanFilename($value)
    {
        $pattern        = '/[^a-zA-Z0-9\-_\.& \(\)]+/';
        $replacement    = '-';
        $value          = preg_replace($pattern, $replacement, $value);

        return $value;
    }
}
