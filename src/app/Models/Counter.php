<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    protected $table        = 'counters';
    protected $fillable     = ['province_id'
        ,'dimension_id'
        ,'max_value'
        ,'min_value'
    ];

    public function dimension()
    {
        return $this->belongsTo('App\Models\Dimension','dimension_id');
    }

}
