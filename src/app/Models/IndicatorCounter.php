<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class IndicatorCounter extends Model
{
    protected $table        = 'indicator_counters';
    protected $fillable     = ['province_id'
        ,'indicator_id'
        ,'max_value'
        ,'min_value'
    ];
}
