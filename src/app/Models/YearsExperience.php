<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YearsExperience extends Model
{
    protected $table        = 'years_experience';
    protected $fillable     = ['name'
        ,'is_active'
    ];
}
