<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    protected $table        = 'indicators';
    protected $fillable     = ['dimension_id'
        ,'name'
        ,'is_active'
    ];

    public function getNameAttribute($value)
	{
	    return ucwords($value);
    }

    public function dimension()
    {
        return $this->belongsTo('App\Models\Dimension','dimension_id');
    }

    public function questionnaire()
    {
        return $this->hasMany('App\Models\Questionnaire');
    }

    static function getTotal($id,$user_id,$test_id)
    {
        return DB::select(db::raw("
            SELECT indicators.id,
                COALESCE(sum(dtl.total),'0') as total from indicators
            LEFT JOIN 
            (
                SELECT questionnaires.indicator_id
                    ,sum(COALESCE(dtl.total,'0')) as total
                FROM questionnaires
                LEFT JOIN 
                (
                    SELECT questionnaire_id,
                        count(0) as total
                    FROM test_questionnaire
                    WHERE answer = 't'
                    AND EXISTS
                    (
                        SELECT 1 
                        FROM tests
                        WHERE tests.id = test_questionnaire.test_id
                        AND user_id = $user_id
                        AND tests.test_number = '$test_id'
                    )
                    GROUP BY questionnaire_id
                ) dtl on dtl.questionnaire_id = questionnaires.id
                WHERE questionnaires.is_active = true
                GROUP BY questionnaires.indicator_id
            ) dtl on dtl.indicator_id = indicators.id
            WHERE indicators.id = $id
            AND indicators.is_active = true
            GROUP BY indicators.id
            ORDER by dimension_id asc
        "));
    }
}
