<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_counters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('indicator_id');
            $table->integer('max_value')->default(0);
            $table->integer('min_value')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_counters');
    }
}
