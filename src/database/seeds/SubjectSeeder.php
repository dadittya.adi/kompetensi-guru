<?php

use Illuminate\Database\Seeder;

use App\Models\Subject;
use App\Models\SchoolLevel;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sd = SchoolLevel::where('name','SD')->first();
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'Bahasa Indonesa',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'Pendidikan Kewarganegaraan',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'Matematika',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'Seni Budaya dan Prakarya (SBDP)',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'Pendidikan Jasmani, Olahraga dan Kesehatan (PJOK)',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'IPA',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'IPS',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sd->id,
            'name' => 'MP Tambahan',
            'is_active' => true,
        ]);

        $smp = SchoolLevel::where('name','SMP')->first();
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Agama',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Pancasila dan Kewarganegaraan',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Bahasa Indonesia',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Matematika',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'IPA',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'IPS',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Bahasa Inggris',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Seni Budaya',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Pendidikan Jasmani',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $smp->id,
            'name' => 'Prakarya',
            'is_active' => true,
        ]);

        $sma = SchoolLevel::where('name','SMA')->first();
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Bahasa Inggris',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Seni Budaya dan Muatan Lokal',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Prakarya',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Biologi',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Fisika',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Kimia',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Sejarah',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Sosiologi',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Ekonomi',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Geografi',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Bahasa Mandarin',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Bahasa Perancis',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Bahasa Jerman',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Komputer',
            'is_active' => true,
        ]);
        Subject::create([
            'school_level_id' => $sma->id,
            'name' => 'Muatan Lokal',
            'is_active' => true,
        ]);
    }
}
