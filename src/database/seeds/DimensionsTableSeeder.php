<?php

use Illuminate\Database\Seeder;
use App\Models\Dimension;
use App\Models\Indicator;
use Laravolt\Indonesia\Models\Province;
use App\Models\Counter;
use App\Models\IndicatorCounter;

class DimensionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $_dimension = Dimension::create([
            'name'      => "kompetensi pedagogic",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/5oRyePqCuj0?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "mastery of learning theory",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/UD4_jY2LtuU?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "mastery of learning method",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/llX54sslZ18?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi kepribadian",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/C-TkF2KWFLU?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "value",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/Gs6mXGsm-GI?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "work ethic",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/lU3G7OJ0y3w?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi sosial",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/lQBeVlHzasU?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "customer service orientation",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/zuUVK3gtASc?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "relationship building",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/wB21KKnKkpA?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi profesional",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/Fo8mw6oNbxM?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "task performance",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/5pAQOJOBr28?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "reflective action",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/RmCqupOBlCQ?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi methodology",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/_gljXMOoI3Y?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "problem solving",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/488l4cdAGUY?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "research skill",
            'is_active'     => true,
            'video_url'     => "https://www.youtube.com/embed/65jPwiuyK0M?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi it",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/Yvtc1mKjLjA?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "mastery of technology access",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "mastery of technology media skills",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi impact and influence",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/zGetBf2UdtI?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "sustainable mindset",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "personalized power",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi achievement orientation",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/VxskPJ348vU?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "ability to work under pressure",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "focus on improvement",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi nasionalism",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/qtqQBwuHmks?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "prioritizing the nation",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "accept pluralism",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        $_dimension = Dimension::create([
            'name'      => "kompetensi developing others",
            'is_active' => true,
            'video_url' => "https://www.youtube.com/embed/y0iE6KqCO9g?list=PLGB6JrA8LCPi2uIO7kYfXB15qfvGu5ED5"
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "leadership skills",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        Indicator::create([
            'dimension_id'  => $_dimension->id,
            'name'          => "ability to transfer knowledge",
            'is_active'     => true,
            'video_url'     => ""
        ]);

        $dimesions = Dimension::all();

        foreach($dimesions as $key => $dimension) {
            $dimension_id = $dimension->id;

            $provinces = Province::all();

            foreach ($provinces as $key => $province) {
                $is_counter_exist = Counter::where('dimension_id', $dimension_id)
                                        ->where('province_id', $province->id)
                                        ->first();
                if (!$is_counter_exist) {
                    Counter::create([
                        'dimension_id' => $dimension_id,
                        'province_id' => $province->id,
                        'max_value' => 0,
                        'min_value' => 0
                    ]);
                }
            }
        }

        $indicators = Indicator::all();

        foreach($indicators as $key => $indicator) {
            $indicator_id = $indicator->id;

            $provinces = Province::all();

            foreach ($provinces as $key => $province) {
                $is_counter_exist = IndicatorCounter::where('indicator_id', $indicator_id)
                                        ->where('province_id', $province->id)
                                        ->first();
                if (!$is_counter_exist) {
                    IndicatorCounter::create([
                        'indicator_id' => $indicator_id,
                        'province_id' => $province->id,
                        'max_value' => 0,
                        'min_value' => 0
                    ]);
                }
            }
        }

    }
}
