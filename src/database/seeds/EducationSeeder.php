<?php

use Illuminate\Database\Seeder;

use App\Models\Education;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Education::create([
            'name' => 'SMA',
            'is_active' => true
        ]);
        Education::create([
            'name' => 'S1',
            'is_active' => true
        ]);
        Education::create([
            'name' => 'S2',
            'is_active' => true
        ]);
        Education::create([
            'name' => 'S3',
            'is_active' => true
        ]);
    }
}
