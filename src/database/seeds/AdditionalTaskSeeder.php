<?php

use App\Models\AdditionalTask;
use Illuminate\Database\Seeder;

class AdditionalTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdditionalTask::create([
            'name'      => 'Kepala Perpustakaan',
            'is_active' => true,
        ]);

        AdditionalTask::create([
            'name'      => 'Kepala Laboratorium',
            'is_active' => true,
        ]);

        AdditionalTask::create([
            'name'      => 'Ketua Program Keahlian',
            'is_active' => true,
        ]);

        AdditionalTask::create([
            'name'      => 'Wali Kelas',
            'is_active' => true,
        ]);

        AdditionalTask::create([
            'name'      => 'Pembina OSIS,',
            'is_active' => true,
        ]);

        AdditionalTask::create([
            'name'      => 'Pembina Ekstrakurikuler',
            'is_active' => true,
        ]);

        AdditionalTask::create([
            'name'      => 'Koordinator PPKB / PKG / BKK',
            'is_active' => true,
        ]);
        AdditionalTask::create([
            'name'      => 'Penilai Kinerja Guru',
            'is_active' => true,
        ]);
        AdditionalTask::create([
            'name'      => 'Guru Piket',
            'is_active' => true,
        ]);
        AdditionalTask::create([
            'name'      => 'Ketua lembaga sertifikasi profesi pihak pertama (LPS-P1)',
            'is_active' => true,
        ]);
        AdditionalTask::create([
            'name'      => 'Pengurus organisasi/asosiasi profesi guru',
            'is_active' => true,
        ]);
    }
}
