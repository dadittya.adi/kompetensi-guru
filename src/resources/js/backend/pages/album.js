$(document).ready( function ()
{ 
    var table = $('#dtTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:25,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/backend/album/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'title', name: 'title',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:true,orderable:true},
        ]
    });

    var dtTable = $('#dtTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtTable.draw();

    $('#form').submit(function (event)
    {
        event.preventDefault();
        var title            = $('#name').val();
       
        if(!title)
        {
            $("#alert_warning").trigger("click", 'Please type title first');
            return false
        }
        
        $('#formModal').modal('toggle');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () 
                    {
                        setToInsert();
                        $("#alert_success").trigger("click", 'Data successfully saved');
                        $('#dtTable').DataTable().ajax.reload();
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        $('#formModal').modal();
                    }
                });
            }
        });
    });
});

function edit(url)
{
    $.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
        success: function () 
        {
			$.unblockUI();
		},
	})
    .done(function (data) 
    {
        $('#title').text('Update Album');
        $('#name').val(data.title);
        $('#form').attr('action', data.url_update);
		$('#formModal').modal();
        
        
	});
}

function setToInsert()
{
    var url_insert =  $('#url_insert').val();
    $('#title').text('Create Album');
    $('#name').val('');
    $('#form').attr('action', url_insert);
    $("#formModal .close").click()
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Data successfully deleted');
        $('#dtTable').DataTable().ajax.reload();
    });
}
