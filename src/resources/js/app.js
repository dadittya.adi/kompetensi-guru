
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('maps', require('./components/Maps.vue').default);
Vue.component('form-login', require('./components/Login.vue').default);
Vue.component('quiz-question', require('./components/QuizQuestion.vue').default);

Vue.component('select-province', require('./components/SelectProvince.vue').default);
Vue.component('select-city', require('./components/SelectCity.vue').default);
Vue.component('select-subject', require('./components/SelectSubject.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo);

// You can also pass in the default options
Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
});

import { store } from './store';

const app = new Vue({
    store,
    el: '#app',
    data: {
        subjects: [],
        isPasswdShow: false
    },
    methods: {
        loadMoreArticles() {
            axios.get('/api/articles/')
                .then((res) => {
                    console.log(res.data);
                });
        },
        loadSubject() {
            axios.get(`/api/subjects/${this.$refs.school_level.value}`)
                .then( res => {
                    this.subjects = res.data;
                });
        },
        showPasswd() {
            if (this.isPasswdShow) {
                this.$refs.password.type = 'password';
                this.isPasswdShow = false;
            } else {
                this.$refs.password.type = 'text';
                this.isPasswdShow = true;
            }
        },
    }
});
