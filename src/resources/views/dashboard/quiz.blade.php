@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Tes',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <quiz-question></quiz-question>
        </div>
    </div>
</div>
@endsection
