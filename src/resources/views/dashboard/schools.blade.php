@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Sekolah Tempat Mengajar',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        @include('_partials.dashboard-sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-green font-weight-bold d-flex align-items-center">Data Sekolah Tempat Mengajar</h5>
                    <hr>
                    <form action="{{ route('school.update') }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="school_level" class="font-weight-bold">Jenjang Sekolah</label>
                                <select name="school_level" id="school_level" class="form-control" @change="loadSubject()" ref="school_level">
                                    <option value="">- Pilih Jenjang Sekolah -</option>
                                    @foreach(App\Models\SchoolLevel::get() as $school_level)
                                        <option value="{{ $school_level->id }}"
                                            @if(old('school_level') == $school_level->id || Auth::user()->school_level_id == $school_level->id )
                                                selected
                                            @endif
                                        >{{ $school_level->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('school_level'))
                                    <small class="form-text text-danger">{{ $errors->first('school_level') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="status" class="font-weight-bold d-block">Status Sekolah</label>
                                <div class="custom-control custom-radio custom-control-inline py-1">
                                    <input type="radio" class="custom-control-input" id="negeri" value="Negeri" name="status" checked required>
                                    <label class="custom-control-label" for="negeri">Negeri</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline py-1">
                                    <input type="radio" class="custom-control-input" id="swasta" value="Swasta" name="status" required>
                                    <label class="custom-control-label" for="swasta">Swasta</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="school_name" class="font-weight-bold">Nama Sekolah</label>
                                <input type="text" class="form-control" id="school_name" name="school_name" value="{{ old('school_name') ? old('school_name') : Auth::user()->school_name }}">
                                @if($errors->has('school_name'))
                                    <small class="form-text text-danger">{{ $errors->first('school_name') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="province" class="font-weight-bold">Provinsi</label>
                                <select-province :provinces="{{ $provinces }}" :selected-province="'{{ Auth::user()->province }}'"></select-province>
                                @if($errors->has('province'))
                                    <small class="form-text text-danger">{{ $errors->first('province') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="city" class="font-weight-bold">Kota/Kabupaten</label>
                                <select-city :default-cities="{{ Auth::user()->province ? \Indonesia::findProvince(Auth::user()->province, ['cities'])->cities : 0 }}" :selected-city="'{{ Auth::user()->city }}'"></select-city>
                                @if($errors->has('city'))
                                    <small class="form-text text-danger">{{ $errors->first('city') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="teacher_type" class="font-weight-bold">Jenis Guru</label>
                                <select name="teacher_type" id="teacher_type" class="form-control">
                                    <option value="">- Pilih Jenis Guru -</option>
                                    @foreach(App\Models\TeacherType::get() as $teacher_type)
                                        <option value="{{ $teacher_type->id }}"
                                            @if(old('teacher_type') == $teacher_type->id || Auth::user()->teacher_type_id == $teacher_type->id )
                                                selected
                                            @endif
                                        >{{ $teacher_type->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('teacher_type'))
                                    <small class="form-text text-danger">{{ $errors->first('teacher_type') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="subject" class="font-weight-bold">Mata Pelajaran Utama</label>
                                <select-subject :default-subjects="{{ App\Models\Subject::where('school_level_id', Auth::user()->school_level_id)->get() }}" :subjects="subjects" :selected-subject="'{{ Auth::user()->subject_id }}'"></select-subject>
                                @if($errors->has('subject'))
                                    <small class="form-text text-danger">{{ $errors->first('subject') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="experience" class="font-weight-bold">Lama Mengajar</label>
                                <select name="experience" id="experience" class="form-control">
                                    <option value="">- Pilih Lama Mengajar -</option>
                                    @foreach(App\Models\YearsExperience::get() as $experience)
                                        <option value="{{ $experience->id }}"
                                            @if(old('experience') == $experience->id || Auth::user()->years_experience_id == $experience->id )
                                                selected
                                            @endif
                                        >{{ $experience->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('experience'))
                                    <small class="form-text text-danger">{{ $errors->first('experience') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="certification" class="font-weight-bold">Sertifikasi</label>
                                <select name="certification" id="certification" class="form-control">
                                    @foreach(['Telah Sertifikasi', 'Belum Sertifikasi'] as $index => $certification)
                                        <option value="{{ $certification }}"
                                            @if(old('certification') == $certification || Auth::user()->certification == $certification )
                                                selected
                                            @endif
                                        >{{ $certification }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('certification'))
                                    <small class="form-text text-danger">{{ $errors->first('certification') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="additional_task" class="font-weight-bold">Tugas Tambahan</label>
                                <select name="additional_task" id="additional_task" class="form-control">
                                    <option value="">- Pilih Tugas Tambahan -</option>
                                    @foreach(App\Models\AdditionalTask::get() as $additional_task)
                                        <option value="{{ $additional_task->id }}"
                                            @if(old('additional_task') == $additional_task->id || Auth::user()->additional_task_id == $additional_task->id )
                                                selected
                                            @endif
                                        >{{ $additional_task->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col"></div>
                        </div>
                        <div class="text-right">
                            <button class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
