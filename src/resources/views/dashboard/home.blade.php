@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Profil Saya',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('home'),
        ]
    ],
])
<div class="container py-5">
    <div class="row justify-content-center">
        @include('_partials.dashboard-sidebar')
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-green font-weight-bold d-flex align-items-center">Biodata Diri</h5>
                    <hr>
                    <form action="{{ route('account.update') }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="name" class="font-weight-bold">Nama Lengkap</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ? old('name') : Auth::user()->name }}">
                                @if( $errors->has('name') )
                                    <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="gender" class="font-weight-bold d-block">Jenis Kelamin</label>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="genderMale" value="pria" name="gender" {{ Auth::user()->gender == 'pria' ? 'checked' : '' }} required>
                                    <label class="custom-control-label has-icn d-flex align-items-center" for="genderMale"><i class="icn icn-male w-2x mr-2"></i> Laki-laki</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="genderFemale" value="wanita" name="gender" {{ Auth::user()->gender == 'wanita' ? 'checked' : '' }} required>
                                    <label class="custom-control-label has-icn d-flex align-items-center" for="genderFemale"><i class="icn icn-female w-2x mr-2"></i> Perempuan</label>
                                </div>
                                @if( $errors->has('gender') )
                                    <small class="form-text text-danger">{{ $errors->first('gender') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="age" class="font-weight-bold">Usia</label>
                                <select name="age" id="age" class="form-control">
                                    @foreach(App\Models\AgeCategory::get() as $age)
                                        <option value="{{ $age->id }}" {{ Auth::user()->age_category_id == $age->id ? 'selected' : '' }}>{{ $age->name }}</option>
                                    @endforeach
                                </select>
                                @if( $errors->has('age') )
                                    <small class="form-text text-danger">{{ $errors->first('age') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="education" class="font-weight-bold">Latar Belakang Pendidikan</label>
                                <select name="education" id="education" class="form-control">
                                    @foreach(App\Models\Education::get() as $education)
                                        <option value="{{ $education->id }}" {{ Auth::user()->education_id == $education->id ? 'selected' : '' }}>{{ $education->name }}</option>
                                    @endforeach
                                </select>
                                @if( $errors->has('education') )
                                    <small class="form-text text-danger">{{ $errors->first('education') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="nuptk" class="font-weight-bold">Nomor NUPTK</label>
                                <input type="text" class="form-control" id="nuptk" name="nuptk" value="{{ old('nuptk') ? old('nuptk') : Auth::user()->nuptk }}">
                                @if( $errors->has('nuptk') )
                                    <small class="form-text text-danger">{{ $errors->first('nuptk') }}</small>
                                @endif
                            </div>
                            <div class="col">
                                <label for="nik" class="font-weight-bold">NIK</label>
                                <input type="text" class="form-control" id="nik" name="nik" value="{{ old('nik') ? old('nik') : Auth::user()->nik }}">
                                @if( $errors->has('nik') )
                                    <small class="form-text text-danger">{{ $errors->first('nik') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="form-group form-row">
                            <div class="col">
                                <label for="employement_status" class="font-weight-bold">Status Kepegawaian</label>
                                <select name="employement_status" id="employement_status" class="form-control">
                                    @foreach(App\Models\EmploymentStatus::get() as $employment)
                                        <option value="{{ $employment->id }}" {{ Auth::user()->age == $age->id ? 'selected' : '' }}>{{ $employment->name }}</option>
                                    @endforeach
                                </select>
                                @if( $errors->has('employement_status') )
                                    <small class="form-text text-danger">{{ $errors->first('employement_status') }}</small>
                                @endif
                            </div>
                            <div class="col"></div>
                        </div>
                        <hr>
                        <div class="text-right">
                            <button class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
