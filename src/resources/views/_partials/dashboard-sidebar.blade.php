<div class="col-md-3">
    <div class="card">
        <div class="card-body p-0">
            <h5 class="text-green font-weight-bold d-flex align-items-center px-3 mb-0 pt-3"><i class="icn icn-male active w-2x mr-2"></i> {{ Auth::user()->name }}</h5>
            <hr class="mb-2">
            <div class="list-menu pb-3">
                <a class="list-menu-item px-3 py-2 {{ request()->routeIs('history') ? 'active' : '' }}" href="{{ route('history') }}">Histori Tes</a>
                <div class="list-menu-divider"></div>
                <a class="list-menu-item px-3 py-2 {{ request()->routeIs('home') ? 'active' : '' }}" href="{{ route('home') }}">Akun Saya</a>
                <a class="list-menu-item px-3 py-2 {{ request()->routeIs('changePassword') ? 'active' : '' }}" href="{{ route('changePassword') }}">Ubah Kata Sandi</a>
                <a class="list-menu-item px-3 py-2 {{ request()->routeIs('schools') ? 'active' : '' }}" href="{{ route('schools') }}">Data Sekolah Tempat Mengajar</a>
                <div class="list-menu-divider"></div>
                <a class="list-menu-item px-3 py-2 {{ request()->routeIs('terms') ? 'active' : '' }}" href="{{ route('terms') }}">Ketentuan Pengguna</a>
                <a class="list-menu-item px-3 py-2 {{ request()->routeIs('policy') ? 'active' : '' }}" href="{{ route('policy') }}">Kebijakan Privasi</a>
            </div>
        </div>
    </div>
</div>
