<section class="bg-green">
    <div class="container text-white">
        <div class="row py-5">
            <div class="col-md-4">
                <h3 class="font-weight-bold">Kompetensi Guru </h3>
                <p class="text-sm py-2">Jl. Kyai H. Syahdan No.9, RT.6/RW.12, Palmerah, Kec. Palmerah, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11480</p>
                <div class="d-flex mx-n3 mb-4">
                    {{-- <div class="px-3">
                        <h6>KONTAK</h6>
                        <p class="text-sm">024 - 572 572</p>
                    </div> --}}
                    <div class="px-3">
                        <h6>EMAIL</h6>
                        <p class="text-sm">sasmoko@binus.edu</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 px-md-5">
                <div class="d-flex mx-n4 mb-4 text-sm">
                    <ul class="list-unstyled mx-4">
                        <li class="py-1"><a href="#" class="text-white">Beranda</a></li>
                        <li class="py-1"><a href="#" class="text-white">Informasi</a></li>
                        <li class="py-1"><a href="#" class="text-white">Artikel</a></li>
                    </ul>
                    <ul class="list-unstyled mx-4">
                        <li class="py-1"><a href="#" class="text-white">Publikasi</a></li>
                        <li class="py-1"><a href="#" class="text-white">Gallery</a></li>
                        <li class="py-1"><a href="#" class="text-white">Tentang Kami</a></li>
                    </ul>
                </div>
                <div class="socmed">
                    <h6>FOLLOW US</h6>
                    <ul class="list-unstyled d-flex mx-n3">
                        <li class="px-3"><a href="#" class="text-white text-lg"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="px-3"><a href="https://www.youtube.com/channel/UCtrGxEk03mbvRtxbWW1O2PQ" class="text-white text-lg"><i class="fab fa-youtube"></i></a></li>
                        <li class="px-3"><a href="https://www.instagram.com/rigedutech_binus/" class="text-white text-lg"><i class="fab fa-instagram"></i></a></li>
                        <li class="px-3"><a href="#" class="text-white text-lg"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <p class="text-center mb-0 pb-3 text-sm">© 2020 kompetensi guru. All rights reserved.</p>
    </div>
</section>
