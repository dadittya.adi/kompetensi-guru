<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        {{-- <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li> --}}
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="{{ asset('images/bg-selamat-datang.jpg') }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-flex flex-column justify-content-center align-items-center mx-auto w-50" style="top:0">
                <h2 class="font-weight-bold">Selamat Datang!</h2>
                <p>Temukan bagaimana kompetensi anda sebagai guru, apakah sudah siap menghadapi dan beradaptasi dengan industri 4.0? Kompetensi Guru dirancang untuk melihat bagaimana gambaran kompetensi guru di Indonesia.</p>
                <a href="{{ route('quiz') }}" class="btn btn-primary">Masuk dan Mulai Tes</a>
            </div>
        </div>
        {{-- <div class="carousel-item">
            <img src="{{ asset('images/bg-selamat-datang.jpg') }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-flex flex-column justify-content-center align-items-center mx-auto w-50" style="top:0">
                <h2 class="font-weight-bold">Selamat Datang!</h2>
                <p>Temukan bagaimana kompetensi anda sebagai guru, apakah sudah siap menghadapi dan beradaptasi dengan industri 4.0? Kompetensi Guru dirancang untuk melihat bagaimana gambaran kompetensi guru di Indonesia.</p>
                <a href="{{ route('quiz') }}" class="btn btn-primary">Masuk dan Mulai Tes</a>
            </div>
        </div>
        <div class="carousel-item">
            <img src="{{ asset('images/bg-selamat-datang.jpg') }}" class="d-block w-100" alt="...">
            <div class="carousel-caption d-flex flex-column justify-content-center align-items-center mx-auto w-50" style="top:0">
                <h2 class="font-weight-bold">Selamat Datang!</h2>
                <p>Temukan bagaimana kompetensi anda sebagai guru, apakah sudah siap menghadapi dan beradaptasi dengan industri 4.0? Kompetensi Guru dirancang untuk melihat bagaimana gambaran kompetensi guru di Indonesia.</p>
                <a href="{{ route('quiz') }}" class="btn btn-primary">Masuk dan Mulai Tes</a>
            </div>
        </div> --}}
    </div>
    {{-- <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> --}}
</div>
