@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Data Publikasi',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <h2 class="text-green font-weight-bold mb-5">Publikasi Peneliti</h2>
        <div class="timeline">
            @foreach( $publications as $publication )
            <div class="timeline-item">
                <div class="timeline-date">
                    <div class="text-center">
                        <h3 class="font-weight-bold mb-0">{{ date('d', strtotime($publication->published_date)) }}</h3>
                        <p class="mb-0">{{ strtoupper(date('M', strtotime($publication->published_date))) }}</p>
                    </div>
                </div>
                <div class="timeline-content">
                    <h2 class="mb-3">{{ \Illuminate\Support\Str::title($publication->title) }}</h2>
                    {{--<div class="text-sm text-muted mb-3">Oleh: {{ strip_tags($publication->title) }}</div>--}}
                    <p class="mb-4">{{ \Illuminate\Support\Str::limit(strip_tags($publication->abstract), 200) }}</p>
                    <a href="{{ route('landing.publikasi.single', $publication->slug) }}" class="btn btn-primary inline-block">Lihat Selengkapnya</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
