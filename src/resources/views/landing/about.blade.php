@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Tentang Kami',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ]
    ],
])
<section class="my-5">
    <div class="container">
        <h2 class="text-success font-weight-bold">Tentang  Kompetensi Guru</h2>
        <p class="mb-5">
            <br> Kompetensi Guru 4.0 dikembangkan oleh RIG Edutech Bina Nusantara University melalui Hibah Kemenristek DIKTI Multiyear Tahun 2019 – 2020 Penelitian Terapan dengan judul “Kompetensi Guru 4.0”
            <h4 class="font-weight-bold">Tim Peneliti</h4>
Prof. Dr. Ir. Sasmoko, M. Pd<br>
Yasinta Indrianti, M. Psi, Psikolog<br>
Yogi Udjaja, S. Kom., M.TI<br><br>

<h4 class="font-weight-bold">Mitra Peneliti</h4>
Prof. Dr. Unifah Rosyidi. M. Pd<br>
Pengurus Besar PGRI Pusat<br>
        </p>
        {{-- <h2 class="text-success font-weight-bold">Mengapa perlu Kompetensi Guru?</h2>
        <p class="mb-5">
            Tata kelola guru di Indonesia harus turut berbenah dalam menghadapi tantangan era industri 4.0. Inkarnasi pendidikan dari era revolusi industri 1.0 menuju 4.0 telah menunjukkan adanya peningkatan tuntutan yang berbeda dan semakin disruptive. Kunci utama untuk mampu mengatasi perubahan adalah pada kecocokan kompetensi guru dengan tuntutan di revolusi industri 4.0 di Indonesia. Saat ini, guru di Indonesia wajib memenuhi tuntutan 4 (empat) kompetensi dasar, yaitu kompetensi pedagogik, kepribadian, sosial dan profesional. Namun, jika melihat tantangan dan tuntutan revolusi di era industri 4.0 yang bercirikan otomatisasi dan digitalisasi, apakah keempat kompetensi guru yang diberlakukan saat ini telah cukup memenuhi gambaran profesionalitas guru Indonesia dalam memasuki era revolusi indistri 4.0?. Oleh karena itu perlu dilakukan kajian kembali tentang Kompetensi Guru Indonesia 4.0, yaitu kompetensi guru yang mampu menghidupi profesionalitas guru pada era industri 4.0.
        </p> --}}
        {{-- <button class="mt-4 d-flex mx-auto btn btn-primary">Load More</button> --}}
    </div>
</section>
@endsection
