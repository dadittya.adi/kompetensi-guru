@extends('layouts.app')

@section('content')
@include('_partials.breadcrumb',[
    'page_title' => 'Profile Peneliti',
    'links' => [
        [
            'name' => 'Beranda',
            'url' => route('landing.home'),
        ],
    ],
])
<section class="my-5">
    <div class="container">
        <div class="list-figured">
            @foreach($researchists as $index => $researchist)
            <div class="figured-item {{ $index % 2 == 0 ? 'figured-left' : 'figured-right' }}">
                <div class="figured-image">
                    <img src="{{ route('landing.image.show',['researcher', $researchist->image]) }}" class="img-fluid" alt="">
                </div>
                <div class="figured-desc">
                    <h2 class="font-weight-bold text-green mb-4">{{ $researchist->name }}</h2>
                    <p class="mb-4">{{ \Illuminate\Support\Str::limit(strip_tags($researchist->content),400) }}</p>
                </div>
            </div>
            @endforeach
        </div>

        <div class="mt-4 d-flex justify-content-center">
            {{ $researchists->links() }}
        </div>
    </div>
</section>
@endsection
