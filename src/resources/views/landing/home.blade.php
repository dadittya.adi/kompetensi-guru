@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ asset('css/maps.css') }}">
@endpush
@section('content')
    {{-- @include('_partials.newsbar') --}}
    @include('_partials.carousel')
    <section class="my-5">
        <div class="container">
            <div class="list-figured">
                <div class="figured-item figured-left">
                    <div class="figured-image">
                        <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="figured-desc">
                        <h2 class="font-weight-bold text-green mb-4 text-right">Apa itu Kompetensi Guru?</h2>
                        <p class="mb-4 text-right">Tata kelola guru di Indonesia harus turut berbenah dalam menghadapi tantangan era industri 4.0. Inkarnasi pendidikan dari era revolusi industri 1.0 menuju 4.0 telah menunjukkan adanya peningkatan tuntutan yang berbeda dan semakin disruptive. Kunci utama untuk mampu mengatasi perubahan adalah pada kecocokan kompetensi guru dengan tuntutan di revolusi industri 4.0 di Indonesia. Saat ini, guru di Indonesia wajib memenuhi tuntutan 4 (empat) kompetensi dasar, yaitu kompetensi pedagogik, kepribadian, sosial dan profesional. Namun, jika melihat tantangan dan tuntutan revolusi di era industri 4.0 yang bercirikan otomatisasi dan digitalisasi, apakah keempat kompetensi guru yang diberlakukan saat ini telah cukup memenuhi gambaran profesionalitas guru Indonesia dalam memasuki era revolusi indistri 4.0?. Oleh karena itu perlu dilakukan kajian kembali tentang Kompetensi Guru Indonesia 4.0, yaitu kompetensi guru yang mampu menghidupi profesionalitas guru pada era industri 4.0.</p>
                        {{-- <a href="#" class="btn btn-primary inline-block">Lihat Selengkapnya</a> --}}
                    </div>
                </div>
                {{-- <div class="figured-item figured-right">
                    <div class="figured-image">
                        <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="figured-desc">
                        <h2 class="font-weight-bold text-green mb-4">Apa itu Kompetensi Guru?</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                        <a href="#" class="btn btn-primary inline-block">Lihat Selengkapnya</a>
                    </div>
                </div>
                <div class="figured-item figured-left">
                    <div class="figured-image">
                        <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="figured-desc">
                        <h2 class="font-weight-bold text-green mb-4">Apa itu Kompetensi Guru?</h2>
                        <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec porta bibendum nunc eget blandit. Donec dolor magna, fermentum eget sapien ut, lobortis condimentum ipsum. Nullam rutrum orci in massa feugiat consectetur. Maecenas lobortis lorem id mauris pharetra cursus. </p>
                        <a href="#" class="btn btn-primary inline-block">Lihat Selengkapnya</a>
                    </div>
                </div> --}}
            </div>
            <div class="row">
                <div class="py-4 col-md-12">
                    <h2 class="font-weight-bold text-green text-center mb-5 pb-4">Tujuan Kompetensi Guru</h2>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('images/male_avatar.png') }}" class="img-fluid mb-3" style="width:150px;height:150px;" alt="">
                            <h4>Mengembangkan Kompetensi Guru</h4>
                            <p>Kompetensi Guru 4.0 yang kontekstual untuk Indonesia dan menyiapkan berbagai perangkat untuk mengukur dan menganalisis, sehingga menjadi dasar kebijakan pengembangan kompetensi guru di Indonesia. </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('images/male_avatar.png') }}" class="img-fluid mb-3" style="width:150px;height:150px;" alt="">
                            <h4>Mendukung Tata Kelola Pendidikan</h4>
                            <p>Untuk mendukung tata Kelola pendidikan dalam menyiapkan input, proses maupun outcome sehingga dapat sejalan dengan fenomena transformasi menuju industri 4.0. </p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="{{ asset('images/male_avatar.png') }}" class="img-fluid mb-3" style="width:150px;height:150px;" alt="">
                            <h4>Memenuhi Inkarnasi Pendidikan</h4>
                            <p>Untuk memenuhi inkarnasi pendidikan yang mengarah pada pendidikan 4.0 sehingga guru mampu memenuhi tuntutan dalam memodifikasi dan mengembangkan kompetensi yang dimiliki </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-green">
        <div class="container text-white text-center p-5">
            <h4 class="font-weight-bold">Kembangkan Kompetensi Anda</h4>
            <p>Untuk dapat mencapai tujuan, maka perlu sebuah proses untuk menganalisa kompetensi yang dimiliki.</p>
            <a href="#" class="btn btn-primary inline-block">Daftar Sekarang</a>
        </div>
    </section>

    <section class="bg-blue-light">
        <div class="container py-5">
            <h2 class="text-center text-green font-weight-bold mb-5">Sebaran hasil Tes Kompetensi <br> di Indonesia</h2>
            <maps></maps>
        </div>
    </section>

    <section>
        <div class="container py-5">
            <h2 class="text-center text-green font-weight-bold mb-2">Artikel Terbaru</h2>
            <p class="text-center mb-5">Artikel, berita dan kegiatan seputar Kompetensi Guru</p>
            <div class="row">
                @foreach( $articles as $article )
                    <div class="col-md-4">
                        <div class="post-thumbnail">
                            <img src="{{ asset('images/login_cover.jpg') }}" class="img-fluid" alt="">
                            <h4 class="post-title my-3"><a href="{{ route('landing.artikel.single', $article->slug) }}">{{ $article->title }}</a></h4>
                            <div class="text-sm text-muted mb-3">Posted on {{ date("M d, Y", strtotime($article->created_at)) }}</div>
                            <p>
                                {{ \Illuminate\Support\Str::limit(strip_tags(str_replace("&nbsp;", "", $article->content)),100) }}
                                <a href="{{ route('landing.artikel.single', $article->slug) }}" class="text-sm">Read More  ></a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                <a href="{{ route('landing.artikel') }}" class="mt-4 btn btn-primary">Lihat Semua</a>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src="{{ asset('js/maps.js') }}"></script>
@endpush
