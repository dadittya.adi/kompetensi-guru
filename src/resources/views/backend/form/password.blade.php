<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }} col-lg-12">
	@if (isset($label))
		<label
			for="{{ $field }}" class="control-label text-semibold {{ isset($label_col) ? $label_col : 'col-lg-2' }}"
		>
			{{ $label }}
		</label>
	@endif
	<div class="input-group {{ isset($form_col) ? $form_col : 'col-lg-10' }}">
		{!! 
			Form::password(
				$field,
				[
					'class' => 'form-control ' . (isset($class) ? $class : ''),
					'placeholder' => isset($placeholder) ? $placeholder : '',
					'autocomplete' => 'off'
				] + (isset($attributes) ? $attributes : [])
			)
		!!}

		<div class="input-group-btn">
			<button class="btn btn-default" type="button" id="{{ $field }}Edit" title="edit">
				<span class="icon-pencil6"></span>
			</button>
		</div>
	</div>

	@if (isset($help))
	<span class="help-block">{{ $help }}</span>
	@endif
	@if (isset($mandatory))
		<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
	@endif
	@if ($errors->has($field))
	<span class="help-block text-danger">{{ $errors->first($field) }}</span>
	@endif
</div>